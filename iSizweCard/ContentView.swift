import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            Color(red: 0.15, green: 0.68, blue: 0.38)
                .edgesIgnoringSafeArea(.all)
            VStack {
                ProfileImageView(imageName: "gir_profile")
                
                Text("Isizwe Madalane")
                    .font(Font.custom("Lobster-Regular", size: 40))
                    .bold()
                    .foregroundColor(.white)
                
                Text("iOS Developer")
                    .foregroundColor(.white)
                    .font(.system(size: 25))
                
                Divider()
                
                InfoView(
                    text: "+27 12 345 6789",
                    imageName: "phone.fill")
                InfoView(
                    text: "isizwe@madalane.com",
                    imageName: "phone.fill")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
