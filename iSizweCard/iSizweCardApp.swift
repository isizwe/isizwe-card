import SwiftUI

@main
struct iSizweCardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
